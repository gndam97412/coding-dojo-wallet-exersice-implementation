package com.codingdojo.wallet.services;

import com.codingdojo.wallet.models.Value;
import com.codingdojo.wallet.models.Wallet;
import com.codingdojo.wallet.enums.CurrencyEnum;
import com.codingdojo.wallet.enums.StockTypeEnum;

public interface WalletService {
    /**
     * Method to create and calculate the value of a wallet
     * @param stockNumber the stock number in the wallet
     * @param stockType the stockType
     * @param currency the currency to get the walue value in
     * @return the value of the wallet in the target currency
     * @throws Exception Exception when an error occures while retrieving the exchange rate between the wallet and target currency
     */
    Value createAndGetWalletValue(int stockNumber, StockTypeEnum stockType, CurrencyEnum currency) throws Exception;
}
