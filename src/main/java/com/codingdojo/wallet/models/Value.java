package com.codingdojo.wallet.models;

import com.codingdojo.wallet.enums.CurrencyEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Value {
    private CurrencyEnum currency;
    private double value;
}
