package com.codingdojo.wallet.models;

import com.codingdojo.wallet.enums.StockTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Stock {
    private int stockNumber;
    private StockTypeEnum stockType;
}
