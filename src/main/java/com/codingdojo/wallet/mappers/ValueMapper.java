package com.codingdojo.wallet.mappers;

import com.codingdojo.wallet.dtos.ValueDto;
import com.codingdojo.wallet.models.Value;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ValueMapper {

    @BeforeMapping
    static void setDtoCurrency(Value value, @MappingTarget ValueDto valueDto) {
        valueDto.setCurrency(value.getCurrency().name());
    }

    ValueDto toDto(Value value);
}
