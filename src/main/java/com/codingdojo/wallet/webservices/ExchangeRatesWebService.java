package com.codingdojo.wallet.webservices;

import com.codingdojo.wallet.wsresponses.ExchangeRateApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Objects;

@Service
public class ExchangeRatesWebService {

    private final WebClient webClient;
    private static final String API_KEY = "49c5ecbbf717c2e91d33b5543d206b25";

    @Autowired
    public ExchangeRatesWebService(WebClient.Builder builder) {
        this.webClient = builder
                .baseUrl("http://api.exchangeratesapi.io/v1")
                .build();
    }

    /**
     * Method to call the ExchangeRateAPi and retrieve the rate between the main currency and the available currencies of the api
     * @param mainCurrency the main currency
     * @return the api response object
     */
    private ExchangeRateApiResponse getExchangeRates(String mainCurrency) {
        ExchangeRateApiResponse response = webClient.get().uri(uriBuilder -> uriBuilder
                .path("/latest")
                .queryParam("access_key", API_KEY)
                .queryParam("base", mainCurrency)
                .build())
                .retrieve().bodyToMono(new ParameterizedTypeReference<ExchangeRateApiResponse>() {
                }).block();
        if (Objects.nonNull(response)) {
            return response;
        }
        return new ExchangeRateApiResponse();
    }

    /**
     * Method to get the rate exchange between the main currency and the target currency
     * @param mainCurrency the main currency
     * @param targetCurrency the target currency
     * @return the rate between the two currencies
     * @throws Exception when the rate between the two currencies cannot be found
     */
    public double getRateExchange(String mainCurrency, String targetCurrency) throws Exception {
        ExchangeRateApiResponse response = getExchangeRates(mainCurrency);

        if (Objects.nonNull(response) && !CollectionUtils.isEmpty(response.getRates())) {
            Double foundExchangeRate = response.getRates().get(targetCurrency);
            if (Objects.nonNull(foundExchangeRate)) {
                return foundExchangeRate;
            }
        }

        throw new Exception("The exchange rate between " + mainCurrency + " and " + targetCurrency + " cannot be retrieved");
    }
}
