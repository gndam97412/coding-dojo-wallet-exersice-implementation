package com.codingdojo.wallet.dtos;

import lombok.Data;

@Data
public class ValueDto {
    private String currency;
    private double value;
}
