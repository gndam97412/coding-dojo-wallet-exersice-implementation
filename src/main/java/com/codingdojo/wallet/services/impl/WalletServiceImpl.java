package com.codingdojo.wallet.services.impl;


import com.codingdojo.wallet.enums.CurrencyEnum;
import com.codingdojo.wallet.enums.StockTypeEnum;
import com.codingdojo.wallet.models.Stock;
import com.codingdojo.wallet.models.Value;
import com.codingdojo.wallet.models.Wallet;
import com.codingdojo.wallet.services.WalletService;
import com.codingdojo.wallet.webservices.ExchangeRatesWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.codingdojo.wallet.enums.CurrencyEnum.getStockTypeMatchingCurrency;

@Service
public class WalletServiceImpl implements WalletService {

    private final ExchangeRatesWebService exchangeRatesWebService;

    @Autowired
    public WalletServiceImpl(ExchangeRatesWebService exchangeRatesWebService) {
        this.exchangeRatesWebService = exchangeRatesWebService;
    }

    /**
     * Method to create a wallet
     * @param stockNumber the stock number in the wallet
     * @param stockType the stockType
     * @return the created wallet
     */
    private Wallet createWallet(int stockNumber, StockTypeEnum stockType) {
        Stock walletStock = new Stock(stockNumber, stockType);

        return new Wallet(walletStock);
    }

    /**
     * Method to get the value of a wallet
     * @param wallet the given wallet
     * @param currency the currency to get the wallet value in
     * @return the value of the wallet
     * @throws Exception when an error occures while retrieving the exchange rate between the wallet and target currency
     */
    private Value getWalletValue(Wallet wallet, CurrencyEnum currency) throws Exception {

        if(currency.equals(CurrencyEnum.UKN)) {
            throw new Exception("The target currency is unknown");
        }

        CurrencyEnum walletCurrency = getStockTypeMatchingCurrency(wallet.getStock().getStockType());

        if(walletCurrency.equals(CurrencyEnum.UKN)) {
            throw new Exception("The currency of the wallet is unknown");
        }
        double exchangeRate = 1.0;

        if(!walletCurrency.equals(currency)) {
            exchangeRate = this.exchangeRatesWebService.getRateExchange(walletCurrency.getCurrency(), currency.getCurrency());
        }

        double walletValue = exchangeRate * wallet.getStock().getStockNumber();
        return new Value(currency, walletValue);
    }

    /**
     * Method to create and calculate the value of a wallet
     * @param stockNumber the stock number in the wallet
     * @param stockType the stockType
     * @param currency the currency to get the walue value in
     * @return the value of the wallet in the target currency
     * @throws Exception Exception when an error occures while retrieving the exchange rate between the wallet and target currency
     */
    @Override
    public Value createAndGetWalletValue(int stockNumber, StockTypeEnum stockType, CurrencyEnum currency) throws Exception {
        return getWalletValue(createWallet(stockNumber, stockType), currency);
    }
}
