package com.codingdojo.wallet.controllers;

import com.codingdojo.wallet.dtos.ValueDto;
import com.codingdojo.wallet.enums.CurrencyEnum;
import com.codingdojo.wallet.enums.StockTypeEnum;
import com.codingdojo.wallet.mappers.ValueMapper;
import com.codingdojo.wallet.models.Value;
import com.codingdojo.wallet.services.WalletService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.Objects;

import static org.springframework.http.ResponseEntity.status;

@RequestMapping(value = "/wallet")
@RestController
public class WalletController {

    WalletService walletService;
    ValueMapper valueMapper;

    @Autowired
    public WalletController(WalletService walletService, ValueMapper valueMapper) {
        this.walletService = walletService;
        this.valueMapper = valueMapper;
    }

    @ApiOperation(value = "getWalletValue", notes = "Endpoint to get the Value of a wallet in a given currency.", nickname = "getWalletValue")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Wallet value calculation successful.", response = ValueDto.class),
            @ApiResponse(code = 400, message = "Bad request."),
            @ApiResponse(code = 500, message = "An unexpected error has occurred.")
            })
    @RequestMapping(method = RequestMethod.GET, value = "/value")
    /**
     * Endpoint to calculate the value of a wallet
     */
    public ResponseEntity getWalletValue(@RequestParam(name="stockNumber") @ApiParam(value = "The stock number in the wallet.") int stockNumber,
                                         @RequestParam(name="stockType") @ApiParam(value = "The stock type in the wallet. Accepted values are bitcoin, Euro and Dollar.", allowableValues = "bitcoin,Euro,Dollar") String stockType,
                                         @RequestParam(name="currency") @ApiParam(value = "The currency the wallet value has to be calculated in. Accepted values are BTC, EUR and USD.", allowableValues = "BTC,EUR,USD") String currency) {

        if (stockNumber < 1) {
            return status(HttpStatus.BAD_REQUEST)
                    .body("The stock number must be greater than 0");
        }

        if (Objects.isNull(stockType) || !StockTypeEnum.isStockType(stockType)) {
            return status(HttpStatus.BAD_REQUEST)
                    .body(MessageFormat.format("The stockType {0} is not supported", stockType));
        }

        if (Objects.isNull(currency) || !CurrencyEnum.isCurrency(currency)) {
            return status(HttpStatus.BAD_REQUEST)
                    .body(MessageFormat.format("The currency {0} is not supported", currency));
        }

        Value computedValue;
        try {
            computedValue = walletService
                    .createAndGetWalletValue(stockNumber, StockTypeEnum.retrieveStockType(stockType),
                            CurrencyEnum.retrieveCurrency(currency));
        } catch (Exception e) {
            return status(HttpStatus.BAD_REQUEST)
                    .body("The value of the wallet cannot be calculated");
        }
        return new ResponseEntity<>(valueMapper.toDto(computedValue), HttpStatus.OK);
    }

}
