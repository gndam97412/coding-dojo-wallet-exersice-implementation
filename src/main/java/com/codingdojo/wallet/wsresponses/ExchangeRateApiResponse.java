package com.codingdojo.wallet.wsresponses;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.util.HashMap;

@Data
public class ExchangeRateApiResponse {
    private boolean success;
    private long timestamp;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate date;
    private HashMap<String, Double> rates;
}
