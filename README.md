# Coding Dojo Wallet exercice

This project is an implementation of the [Coding Dojo Wallet exercice](https://codingdojo.org/kata/Wallet/).

## Choices made

In order to do the exercise, the [Exchange Rate Api](https://exchangeratesapi.io) has been used. The free account created allows only to get exchange rates for Euro. It is possible to change the API key used for one of a paid account that would allow conversion from the other currencies. Another limitation of this API is that it does not support Petroleum so we are limited to Euro, bitcoin and Dollar.

The project uses SpringBoot with Java 11 and exposes a REST API.
In order to test the application, Swagger has been implemented.

## Usage

Launch the SpringBoot application with your IDE, use a command line tool at the root of the project to launch it (mvn spring-boot:run or ./mvnw spring-boot:run on Windows) or build the JAR of the application.
Once the application is launched, you can use the SwaggerUI at http://localhost:8080/swagger-ui.html to try the application

## License
[MIT](https://choosealicense.com/licenses/mit/)