package com.codingdojo.wallet.enums;

import java.util.Arrays;

public enum StockTypeEnum {

    BTC("bitcoin"),
    EUR("Euro"),
    USD("Dollar"),
    UKN("Unknown");

    private final String stockType;

    StockTypeEnum(String stockType) {
        this.stockType = stockType;
    }

    public String getStockType() {
        return this.stockType;
    }

    public static boolean isStockType(String stockType) {
        return Arrays.stream(StockTypeEnum.values()).anyMatch(stockTypeEnum -> stockTypeEnum.getStockType().equalsIgnoreCase(stockType));
    }

    public static StockTypeEnum retrieveStockType(String stockType) {
        return Arrays.stream(StockTypeEnum.values()).filter(stockTypeEnum -> stockTypeEnum.getStockType().equalsIgnoreCase(stockType)).findFirst().orElse(UKN);
    }


}