package com.codingdojo.wallet.enums;

import java.util.Arrays;

public enum CurrencyEnum {
    BTC("BTC"),
    EUR("EUR"),
    USD("USD"),
    UKN("Unknown");

    private final String currency;

    CurrencyEnum(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return this.currency;
    }

    public static boolean isCurrency(String currency) {
        return Arrays.stream(CurrencyEnum.values()).anyMatch(currencyEnum -> currencyEnum.getCurrency().equalsIgnoreCase(currency));
    }

    public static CurrencyEnum retrieveCurrency(String currency) {
        return Arrays.stream(CurrencyEnum.values()).filter(currencyEnum -> currencyEnum.getCurrency().equalsIgnoreCase(currency)).findFirst().orElse(UKN);
    }

    public static CurrencyEnum getStockTypeMatchingCurrency(StockTypeEnum stockType) {

        switch (stockType) {
            case BTC:
                return CurrencyEnum.BTC;
            case EUR:
                return CurrencyEnum.EUR;
            case USD:
                return CurrencyEnum.USD;
            default:
                return CurrencyEnum.UKN;
        }

    }
}
